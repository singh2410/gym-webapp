export const exerciseOptions = {
    method: 'GET',
    headers: {
        'X-RapidAPI-Host': 'exercisedb.p.rapidapi.com',
        'X-RapidAPI-Key': 'c358154d43mshe0b83b60a268bbbp131d61jsnf48a879bdb84',
      
    },
  };

  export const youtubeOptions = {
    method: 'GET',
    headers: {
     'X-RapidAPI-Host': 'youtube-search-and-download.p.rapidapi.com',
     'X-RapidAPI-Key': 'c358154d43mshe0b83b60a268bbbp131d61jsnf48a879bdb84',

    }
  };
export const fetchData = async (url, options) => {
    const response = await fetch(url, options);
    const data = await response.json();

    return data;
}